<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    protected $table = 'checklist';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = true;

    protected $fillable = array(
        'object_domain',
        'object_id',
        'description',
        'is_completed',
        'completed_at',
        'updated_by',
        'due',
        'urgency'
    );

    public function item()
    {
        return $this->hasMany('App\Item');
    }

    public function getIsCompletedAttribute($value) {
        if($value == 0) return false;
        else return true;
    }
}

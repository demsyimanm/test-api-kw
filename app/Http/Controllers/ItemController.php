<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Checklist;
use App\Item;
use Auth;
use App\Service\ApiService;
class ItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $apiService;
    public function __construct(ApiService $apiService)
    { 
        $this->middleware('auth');
        $this->apiService  = $apiService;
    }
    
    public function getItem($id,$item_id)
    {
        $data = Item::where('id', $item_id)->first();
        return $this->apiService->generate($data,'checklists',201);
    }

    public function getItemList($id)
    {
        $data = Checklist::with('item')->where('id', $id)->get();
        return $this->apiService->generateWithItems($data,'checklists',201,$id);
    }

    public function createItem(Request $request,$id)
    {
        $this->validate($request, [
          'description' => 'required'
        ]);
        $create = Item::create(array(
          'description'     => $request->description,
          'updated_by'      => $request->userid,
          'due'             => $request->due,
          'urgency'         => $request->urgency,
          'checklist_id'    => $id
        ));

        if($create){
            return $this->apiService->generate($create,'checklists',201);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    public function updateItem(Request $request,$id,$item_id)
    {
        $this->validate($request, [
          'description' => 'required'
        ]);
        $update = Item::where('id',$item_id)->update(array(
          'description'     => $request->description,
          'updated_by'      => $request->userid,
          'due'             => $request->due,
          'urgency'         => $request->urgency
        ));
        if($update){
            $data = Item::where('id', $item_id)->first();
            return $this->apiService->generate($data,'checklists',201);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    public function deleteItem($id,$item_id)
    {
        $data = Item::where('id', $item_id)->delete();
        if($data){
          return response()->json(['status' => '204'],204);
        }
        else {
          return response()->json(['status' => '404','error' =>  'Not Found'],404);
        }
        
    }

    public function complete()
    {
        $data = Item::select('id','id as item_id','is_completed','checklist_id')->where('is_completed',1)->get();
        return response()->json($data,200);
    }

    public function incomplete()
    {
        $data = Item::select('id','id as item_id','is_completed','checklist_id')->where('is_completed',0)->get();
        return response()->json($data,200);
    }
    
}
<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Checklist;
use Auth;
use App\Service\ApiService;
class ChecklistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $apiService;
    public function __construct(ApiService $apiService)
    { 
        $this->middleware('auth');
        $this->apiService  = $apiService;
    }
    
    public function getChecklist($id)
    {
        $data = Checklist::where('id', $id)->first();
        return $this->apiService->generate($data,'checklists',201);
    }

    public function getChecklistList(Request $request)
    {
        if(!isset($request->page['offset'])){
          $offset = 0;
        } 
        else{
          $offset = $request->page['offset'];
        }
        if(!isset($request->page['limit'])){
          $limit = 10;
        }
        else{
          $limit = $request->page['limit'];
        }

        $data = Checklist::skip($offset)->take($limit)->get();
        $count = Checklist::count();

        $links['first'] = "http://$_SERVER[HTTP_HOST]/test-api-kw2/public/api/checklists?page[limit]=".$limit."&page[offset]=0";
        
        $remaining = $count%$limit;
        $totalPrev = $count-$remaining;
        $links['last'] = "http://$_SERVER[HTTP_HOST]/test-api-kw2/public/api/checklists?page[limit]=".$limit."&page[offset]=".$totalPrev;
        
        $next = $offset+$limit;
        if($next < $count){
          $links['next'] = "http://$_SERVER[HTTP_HOST]/test-api-kw2/public/api/checklists?page[limit]=".$limit."&page[offset]=".$next;
        }
        else{
          $links['next'] = "null";
        }


        if($offset-$limit < 0){
          $links['prev'] = "null";  
        }
        else{
          $prev = $offset-$limit;
          $links['prev'] = "http://$_SERVER[HTTP_HOST]/test-api-kw2/public/api/checklists?page[limit]=".$limit."&page[offset]=".$prev;
        }

        return $this->apiService->generatePagination($data,'checklists',201,$limit,$count,$links);
    }

    public function createChecklist(Request $request)
    {
        $this->validate($request, [
          'object_domain' => 'required',
          'object_id' => 'required',
          'description' => 'required'
        ]);
        $create = Checklist::create(array(
          'object_domain'   => $request->object_domain,
          'object_id'       => $request->object_id,
          'description'     => $request->description,
          'updated_by'      => $request->userid,
          'due'             => $request->due,
          'urgency'         => $request->urgency
        ));

        if($create){
            return $this->apiService->generate($create,'checklists',201);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    public function updateChecklist(Request $request,$id)
    {
        $this->validate($request, [
          'object_domain' => 'required',
          'object_id' => 'required',
          'description' => 'required'
        ]);
        $update = Checklist::where('id',$id)->update(array(
          'object_domain'   => $request->object_domain,
          'object_id'       => $request->object_id,
          'description'     => $request->description,
          'updated_by'      => $request->userid,
          'due'             => $request->due,
          'urgency'         => $request->urgency
        ));
        if($update){
            $data = Checklist::where('id', $id)->first();
            return $this->apiService->generate($data,'checklists',201);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    public function deleteChecklist($id)
    {
        $data = Checklist::where('id', $id)->delete();
        if($data){
          return response()->json(['status' => '204'],204);
        }
        else {
          return response()->json(['status' => '404','error' =>  'Not Found'],404);
        }
        
    }
    
}
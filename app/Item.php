<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = true;

    protected $fillable = array(
        'description',
        'checklist_id',
        'is_completed',
        'completed_at',
        'due',
        'urgency',
        'updated_by'
    );

    public function checklist()
    {
        return $this->belongsTo('App\Checklist');
    }

    public function getIsCompletedAttribute($value) {
        if($value == 0) return false;
        else return true;
    }
}
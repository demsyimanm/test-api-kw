<?php 
namespace App\Service;
class ApiService {
	public function generate($data,$type,$code){
		return response()->json(['data' => 
                                    [
                                      'type'  => $type,
                                      'id'    => $data->id,
                                      'attributes' => $data,
                                    'links' => ["self" => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"]
                                    ]
                                  ],$code); 
  }

  public function generateWithItems($data,$type,$code,$id){
    return response()->json(['data' => 
                                    [
                                      'type'  => $type,
                                      'id'    => $id,
                                      'attributes' => $data,
                                    'links' => ["self" => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"]
                                    ]
                                  ],$code); 
  }

  public function generatePagination($data,$type,$code,$count,$total,$links){
    return response()->json(['meta' => [
                                'count' => $count,
                                'total' => $total
                              ],
                             'links' => [
                                'first' => $links['first'],
                                'last'  => $links['last'],
                                'next'  => $links['next'],
                                'prev'  => $links['prev'],
                             ],
                             'data' => [
                                'type'  => $type,
                                'attributes' => $data,
                                'links' => ["self" => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"]
                              ]
                            ],$code); 
  }


}
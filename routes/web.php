<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/





$router->group(['prefix' => 'api/'], function ($app) {
	$app->get('login/','UserController@authenticate');
    
    $app->group(['prefix' => 'checklists/'], function ($app) {
    	$app->get('/','ChecklistController@getChecklistList');
    	$app->get('/{id}','ChecklistController@getChecklist');
    	$app->post('/','ChecklistController@createChecklist');
    	$app->patch('/{id}','ChecklistController@updateChecklist');
    	$app->delete('/{id}','ChecklistController@deleteChecklist');

    	// ITEM
    	$app->get('/{id}/items','ItemController@getItemList');
    	$app->get('/{id}/items/{item_id}','ItemController@getItem');
    	$app->post('/{id}/items','ItemController@createItem');
    	$app->patch('/{id}/items/{item_id}','ItemController@updateItem');
    	$app->delete('/{id}/items/{item_id}','ItemController@deleteItem');

    	$app->post('/complete','ItemController@complete');
    	$app->post('/incomplete','ItemController@incomplete');

    });
});
# test-api-kw

Langkah - langkah instalasi :
1. Clone dari repo dan buat DB serta file .env
2. Jalankan migrate dan seed
3. Untuk authentikasi, gunakan :
	username : admin
	password : admin
4. Setelah login, Anda akan mendapatkan API Key, lalu gunakan API Key tsb sebagai key untuk memanggil API lainnya dengan meletakkan key tsb pada header request dengan nama 'authorization'
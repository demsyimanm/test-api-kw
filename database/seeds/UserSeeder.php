<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' 		=> 'Super Admin',
            'username'	=> 'admin',
            'password'	=> password_hash('admin',PASSWORD_DEFAULT)	
        ]);
    }
}
